import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { NgForm } from '@angular/forms';
import { BlogCard } from 'src/app/shared/models/blogCard.model';
import { BlogCardService } from 'src/app/shared/services/blogCard.service';

@Component({
  selector: 'app-create-edit-blog',
  templateUrl: './create-edit-blog.component.html',
  styleUrls: ['./create-edit-blog.component.less']
})
export class CreateEditBlogComponent implements OnInit {
  @ViewChild('form') form!: NgForm;
  public isEdit!: boolean;
  public hasChanged = false;
  public blogCard = new BlogCard();
  public Editor = ClassicEditor;
  private prevTitle = this.blogCard.title;
  private prevText = this.blogCard.text;

  constructor(
    public dialogRef: MatDialogRef<CreateEditBlogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private blogCardService: BlogCardService
  ) { }

  ngOnInit(): void {
    this.isEdit = this.data ? true : false;
    this.blogCard.title = this.data ? this.data.blogCard.title : '';
    this.blogCard.text = this.data ? this.data.blogCard.text : '';
  }

  submit() {
    if (!this.isEdit) {
      this.blogCardService.create(this.blogCard);
    } else {
      this.blogCardService.update(this.blogCard, this.data.blogCard.id);
    }
  }



  hasChangedJob() {
    this.hasChanged = this.prevTitle !== this.blogCard.title || this.prevText !== this.blogCard.text;
  }

}

