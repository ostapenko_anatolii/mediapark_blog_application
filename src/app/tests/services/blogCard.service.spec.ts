import { TestDataBlogService } from './../../shared/services/testDataBlog.service';
import { BlogCardService } from 'src/app/shared/services/blogCard.service';

describe('BlogCardService', () => {
  const testService = new TestDataBlogService();
  const blogCardService = new BlogCardService(testService);
  it('isJson should return true from corect Json file', () => {
    const json = '[{"id":"66576442277832a6","date":"2021-07-21T21:05:18.799Z","title":"Shiba Inu 1","text":"The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan."}]'
    const x = blogCardService.isJson(json);
    expect(x).toEqual(true);
  });

  it('is Json should return false if it is Array', () => {
    const array = [{ date: '2021-07-21T21:05:18.799Z', id: '66576442277832a6', text: 'The Shiba Inu is the smallest ...', title: 'Shiba Inu 1' }];
    const x = blogCardService.isJson(array);
    expect(x).toEqual(false);
  });

  it('is Json should return false if it is undefind', () => {
    const array = undefined;
    const x = blogCardService.isJson(array);
    expect(x).toEqual(false);
  });

});
