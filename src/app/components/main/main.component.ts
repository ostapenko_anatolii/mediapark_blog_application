import { BlogCard } from './../../shared/models/blogCard.model';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BlogCardService } from 'src/app/shared/services/blogCard.service';
import { DialogService } from 'src/app/shared/services/dialog.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.less']
})
export class MainComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  private dataSource!: MatTableDataSource<BlogCard>;
  public cards: any;
  public sortBy!: string;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private dialogService: DialogService,
    private blogCardService: BlogCardService
  ) { }


  ngOnInit(): void {
    this.blogCardService.get().subscribe((cards: Array<BlogCard>) => {
      this.updateBlogList(cards);
    });

    this.blogCardService.subscribeOnAddedCard.subscribe((cards: Array<BlogCard>) => {
      this.updateBlogList(cards);
    });
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  public openDialog(blogCard?: BlogCard): void {
    this.dialogService.openDialog(blogCard);
  }

  public deleteBlog(id: string) {
    this.blogCardService.delete(id).subscribe(blogCards => {
      this.updateBlogList(blogCards);
    });
  }

  private updateBlogList(blogCards: Array<BlogCard>): void {
    this.dataSource = new MatTableDataSource<BlogCard>(blogCards);
    this.changeDetectorRef.detectChanges();
    this.dataSource.paginator = this.paginator;
    this.cards = this.dataSource.connect();
  }

  sortDateBy() {
    this.blogCardService.get().subscribe((cards: Array<BlogCard>) => {
      let sortedCards;

      switch (this.sortBy) {
        case 'day':
          sortedCards = cards.sort((cardA, cardB) => new Date(cardA.date).getDate() - new Date(cardB.date).getDate());
          this.updateBlogList(sortedCards);
          break;
        case 'month':
          sortedCards = cards.sort((cardA, cardB) => new Date(cardA.date).getMonth() - new Date(cardB.date).getMonth());
          this.updateBlogList(sortedCards);
          break;
        case 'year':
          sortedCards = cards.sort((a, b) => new Date(a.date).getFullYear() - new Date(b.date).getFullYear());
          this.updateBlogList(sortedCards);
          break;
      }
    });
  }

}


