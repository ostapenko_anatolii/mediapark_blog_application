export interface IBlogCard {
  id: string;
  date: Date;
  title: string;
  text: string;
}
