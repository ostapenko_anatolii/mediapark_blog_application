import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditBlogComponent } from 'src/app/dialogs/create-edit-blog/create-edit-blog.component';
import { BlogCard } from '../models/blogCard.model';

@Injectable({ providedIn: 'root' })
export class DialogService {
  constructor(private dialog: MatDialog) { }

  public openDialog(blogCard?: BlogCard): void {
    let dialogRef;
    if (blogCard) {
      dialogRef = this.dialog.open(CreateEditBlogComponent, {
        data: { blogCard }
      });
    } else {
      dialogRef = this.dialog.open(CreateEditBlogComponent, {});
    }
  }

}
