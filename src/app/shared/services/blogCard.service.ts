import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { BlogCard } from '../models/blogCard.model';
import { TestDataBlogService } from './testDataBlog.service';


@Injectable({ providedIn: 'root' })
export class BlogCardService {
  private blogList = Array<BlogCard>();
  public subscribeOnAddedCard = new Subject<Array<BlogCard>>();

  constructor(
    private testDataBlogService: TestDataBlogService
  ) { }

  public isJson(obj: any) {
    try {
      JSON.parse(obj);
    } catch (e) {
      return false;
    }
    return true;
  }

  private idGenerator() {
    return Math.random().toString(11).slice(2);
  }

  public get(): Observable<Array<BlogCard>> {
    return new Observable<Array<BlogCard>>(observer => {
      if (this.isJson(localStorage.blogList)) {
        this.blogList = JSON.parse(localStorage.blogList);
        observer.next(this.blogList);
        observer.complete();
      } else {
        localStorage.setItem('blogList', JSON.stringify(this.testDataBlogService.testDataBlogs));
        observer.next(this.testDataBlogService.testDataBlogs);
        observer.complete();
      }
    });
  }

  public create(card: BlogCard): void {
    card.id = this.idGenerator();
    card.date = new Date();
    this.blogList.push(card);
    localStorage.setItem('blogList', JSON.stringify(this.blogList));
    this.subscribeOnAddedCard.next(this.blogList);
  }

  public update(card: BlogCard, id: string) {
    this.blogList.map((blog) => {
      if (blog.id === id) {
        blog.title = card.title;
        blog.text = card.text;
      }
    });
    localStorage.setItem('blogList', JSON.stringify(this.blogList));
  }

  public delete(id: string): Observable<Array<BlogCard>> {
    return new Observable<Array<BlogCard>>(observer => {
      this.blogList.map((blog, index) => {
        if (blog.id === id) {
          this.blogList.splice(index, 1);
          localStorage.setItem('blogList', JSON.stringify(this.blogList));
          observer.next(this.blogList);
          observer.complete();
        }
      });
    });
  }
}
