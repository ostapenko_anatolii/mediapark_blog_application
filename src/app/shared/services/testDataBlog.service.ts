import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class TestDataBlogService {
  constructor() { }

  testDataBlogs = [
    {
      id: '66576442277832a6',
      date: new Date(2013, 12, 6),
      title: 'What is Lorem Ipsum?',
      text: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. '
    },
    {
      id: '665764427783d2a6',
      date: new Date(2007, 4, 28),
      title: 'Where does it come from?',
      text: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.'
    },
    {
      id: '66576442666832a6',
      date: new Date(2014, 11, 30),
      title: 'Why do we use it?',
      text: 'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using "Content here, content here", making it look like readable English.'
    },
    {
      id: '665764428d2a6',
      date: new Date(2013, 8, 5),
      title: 'Por qué lo usamos?',
      text: 'Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño. '
    },
    {
      id: '66576442277832a6',
      date: new Date(),
      title: 'Mi a Lorem Ipsum?',
      text: 'A hiedelemmel ellentétben a Lorem Ipsum nem véletlenszerû szöveg. Gyökerei egy Kr. E. 45-ös latin irodalmi klasszikushoz nyúlnak.'
    },
    {
      id: '665764427783d2a6',
      date: new Date(2000, 8, 5),
      title: 'Shiba Inu 7',
      text: 'The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan.'
    },
    {
      id: '66576442666832a6',
      date: new Date(2018, 2, 19),
      title: 'أجده',
      text: ' القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ'
    },
    {
      id: '665764428d2a6',
      date: new Date(2020, 8, 5),
      title: '什么是Lorem Ipsum?',
      text: 'Lorem Ipsum，也称乱数假文或者哑元文本， 是印刷及排版领域所常用的虚拟文字。由于曾经一台匿名的打印机刻意打乱了一盒印刷字体从而造出一本字体样品书，'
    },
    {
      id: '665764422fs7832a6',
      date: new Date(2022, 6, 28),
      title: 'Dove posso trovarlo?',
      text: 'Esistono innumerevoli variazioni dei passaggi del Lorem Ipsum, ma la maggior parte hanno subito delle variazioni del tempo, a causa dell’inserimento di passaggi ironici, o di sequenze casuali di caratteri palesemente poco verosimili.'
    }];
}
