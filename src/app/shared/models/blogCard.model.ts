import { IBlogCard } from '../interfaces/blogCard.interface';

export class BlogCard implements IBlogCard {
  id!: string;
  date!: Date;
  title!: string;
  text!: string;
}
